CREATE TABLE public."session" (
    "key" text NOT NULL,
    value text NULL,
    expiry int8 NULL,
    CONSTRAINT session_pkey PRIMARY KEY (key)
);
CREATE INDEX session_expiry_idx ON public.session USING btree (expiry);
