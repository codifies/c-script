PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "test" (
    "Field1"    INTEGER,
    "Field2"    TEXT,
    "Field3"    INTEGER,
    PRIMARY KEY("Field1" AUTOINCREMENT)
);
INSERT INTO test VALUES(1,'1st',4);
INSERT INTO test VALUES(2,'2nd',3);
INSERT INTO test VALUES(3,'3rd',2);
INSERT INTO test VALUES(4,'4th',1);
DELETE FROM sqlite_sequence;
INSERT INTO sqlite_sequence VALUES('test',4);
COMMIT;
