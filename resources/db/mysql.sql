CREATE DATABASE `test` /*!40100 DEFAULT CHARACTER SET latin1 */;

CREATE TABLE `test` (
  `Field1` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Field2` varchar(100) COLLATE latin1_bin DEFAULT NULL,
  `Field3` int(11) DEFAULT NULL,
  PRIMARY KEY (`Field1`),
  KEY `test_Field1_IDX` (`Field1`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

INSERT INTO test.test (Field1,Field2,Field3) VALUES (1,'1st',4);
INSERT INTO test.test (Field1,Field2,Field3) VALUES (2,'2nd',3);
INSERT INTO test.test (Field1,Field2,Field3) VALUES (3,'3rd',2);
INSERT INTO test.test (Field1,Field2,Field3) VALUES (4,'4th',1);


CREATE TABLE test.sessions (
    sesskey TEXT(40) NULL,
    value varchar(100) NULL,
    expiry BIGINT UNSIGNED DEFAULT 0 NOT NULL
)
ENGINE=InnoDB
COLLATE=utf8_general_ci;
CREATE INDEX session_expiry_IDX USING BTREE ON test.sessions (expiry);
CREATE INDEX session_key_IDX USING BTREE ON test.sessions (sesskey);


