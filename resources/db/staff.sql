DROP TABLE IF EXISTS `staff`;

CREATE TABLE `staff` (
  `id` mediumint(8) unsigned NOT NULL auto_increment,
  `name` varchar(255) default NULL,
  `phone` varchar(100) default NULL,
  `email` varchar(255) default NULL,
  `role` varchar(255) default NULL,
  PRIMARY KEY (`id`)
) AUTO_INCREMENT=1;

INSERT INTO `staff` (`name`,`phone`,`email`,`role`)
VALUES
  ("Rae Rhodes","(184) 612-2012","pede.suspendisse@protonmail.org","2"),
  ("Jessamine Stevens","(276) 647-2158","amet.consectetuer@hotmail.edu","3"),
  ("Ciara Gilbert","1-403-337-9937","elit@aol.ca","3"),
  ("Jolie Combs","1-515-851-6584","cursus.purus@google.couk","2"),
  ("Candice Warren","(825) 144-7721","ultrices.mauris@aol.ca","4"),
  ("Paki Holt","(336) 571-2332","eu.augue.porttitor@yahoo.edu","1"),
  ("Karyn Stuart","(785) 219-9768","iaculis@yahoo.com","1"),
  ("Emmanuel Salas","1-508-238-4654","mauris@aol.com","4"),
  ("Keely Daugherty","1-787-848-6591","sollicitudin.adipiscing@protonmail.couk","3"),
  ("Katell Suarez","(692) 663-3720","nonummy.ultricies.ornare@google.org","3");
INSERT INTO `staff` (`name`,`phone`,`email`,`role`)
VALUES
  ("Linda Barr","(948) 852-6332","non.lorem@aol.net","1"),
  ("Melinda Marshall","1-417-823-2249","libero.est.congue@icloud.net","2"),
  ("Todd Hewitt","1-320-886-5448","pretium@aol.ca","4"),
  ("Buckminster Rodriguez","(986) 516-8658","massa.integer.vitae@protonmail.com","1"),
  ("Travis Miles","1-355-356-7336","et.ultrices@yahoo.org","2"),
  ("Eleanor Roach","1-848-304-1932","orci.luctus.et@outlook.couk","2"),
  ("Nicholas Fitzgerald","1-460-237-8560","sed.pharetra@yahoo.ca","1"),
  ("Reece Ball","1-282-311-5825","enim@hotmail.couk","2"),
  ("Wade Beach","(313) 815-3753","viverra.donec.tempus@protonmail.org","2"),
  ("Clinton Hyde","(818) 860-3642","donec.vitae@protonmail.ca","1");
INSERT INTO `staff` (`name`,`phone`,`email`,`role`)
VALUES
  ("Quinlan Solis","1-396-221-0897","nunc.quisque@hotmail.com","4"),
  ("Claire Warren","1-431-965-2862","convallis@google.ca","2"),
  ("Serina Kirkland","(878) 561-7192","risus.at@outlook.net","3"),
  ("Geraldine Humphrey","(355) 463-1886","dolor@protonmail.edu","4"),
  ("Zoe Jenkins","(783) 298-0636","tempor.arcu@yahoo.org","2"),
  ("Anne Santana","(791) 236-2387","convallis.in@icloud.org","2"),
  ("Maxwell Pierce","1-307-563-5482","iaculis@hotmail.couk","1"),
  ("Cailin Stewart","(458) 588-9454","tempus@aol.couk","3"),
  ("Marsden Nielsen","(869) 785-8153","dictum@aol.net","4"),
  ("Vance Harvey","1-968-564-1981","euismod.urna.nullam@icloud.ca","4");
INSERT INTO `staff` (`name`,`phone`,`email`,`role`)
VALUES
  ("Lars Todd","(277) 745-4981","non.enim.mauris@icloud.couk","2"),
  ("Kermit Calhoun","(255) 573-0348","rutrum.magna@outlook.ca","4"),
  ("Audra Pruitt","1-647-549-4741","a.enim@outlook.net","2"),
  ("Yen Rasmussen","(571) 883-6838","arcu.nunc@yahoo.net","2"),
  ("Amelia Byers","1-276-390-8061","consectetuer@hotmail.couk","1"),
  ("Noel Velez","(453) 145-6222","faucibus.ut.nulla@yahoo.couk","1"),
  ("Nathan Andrews","(674) 832-4148","a.odio@icloud.couk","1"),
  ("Kalia Terry","(287) 543-0636","luctus.ipsum@yahoo.couk","1"),
  ("Emi Dorsey","1-696-355-7207","nibh@protonmail.ca","2"),
  ("Amir Munoz","1-726-546-1526","erat.vivamus.nisi@hotmail.couk","3");
INSERT INTO `staff` (`name`,`phone`,`email`,`role`)
VALUES
  ("Connor Grimes","1-689-552-7351","lobortis.quis@yahoo.edu","4"),
  ("Aiko Bernard","(829) 247-7713","proin.velit.sed@outlook.edu","3"),
  ("Elliott Schroeder","1-368-468-5213","dapibus.gravida@outlook.couk","2"),
  ("Camilla Carpenter","(856) 335-1525","velit.eget@hotmail.ca","1"),
  ("Lyle Rosa","1-788-522-3769","lorem.semper.auctor@protonmail.net","1"),
  ("Melvin Cardenas","1-549-573-3968","egestas@google.com","4"),
  ("Uriah Burch","1-331-384-6521","cursus@outlook.net","3"),
  ("Erich Jackson","(201) 658-8587","fermentum.convallis.ligula@outlook.edu","2"),
  ("Tatyana Dixon","1-225-809-4934","amet.diam@yahoo.edu","4"),
  ("Luke Trevino","(918) 375-8819","ultricies.ornare.elit@icloud.com","3");
INSERT INTO `staff` (`name`,`phone`,`email`,`role`)
VALUES
  ("Henry Sampson","(473) 564-8582","pharetra.nibh.aliquam@outlook.ca","4"),
  ("Nero Woods","1-468-444-5006","quis.diam@outlook.org","4"),
  ("Stone Conley","(624) 479-7427","non.feugiat@aol.couk","3"),
  ("Steven Howell","(516) 182-4552","risus.nunc@google.net","2"),
  ("Tatiana Molina","1-778-739-5984","ut.tincidunt@hotmail.ca","2"),
  ("Athena Delacruz","(727) 736-6874","est.arcu@google.edu","3"),
  ("Colorado Casey","(921) 629-4255","suspendisse.non@google.edu","3"),
  ("Justina Knight","(724) 793-4426","arcu.vestibulum@outlook.org","3"),
  ("Tamekah Webb","1-361-748-8168","convallis@yahoo.ca","4"),
  ("Marsden Dickson","(186) 445-0449","praesent@icloud.net","2");
INSERT INTO `staff` (`name`,`phone`,`email`,`role`)
VALUES
  ("Keane Velez","(796) 724-4393","lorem.eget@icloud.org","2"),
  ("Carly Alford","1-806-150-7765","a.aliquet.vel@yahoo.org","1"),
  ("Bert Brown","1-971-206-9698","vitae.dolor@outlook.ca","1"),
  ("Finn Coffey","(837) 677-6414","pede@hotmail.ca","4"),
  ("Adam Cervantes","(351) 606-8542","ipsum.cursus.vestibulum@icloud.net","1"),
  ("Elizabeth Golden","(571) 772-8485","nec.diam@aol.edu","1"),
  ("Cherokee Skinner","(363) 387-1517","non.lorem@hotmail.edu","3"),
  ("Price Hall","1-232-308-6167","gravida.sit.amet@outlook.com","1"),
  ("Kato Rutledge","1-843-583-8731","arcu.imperdiet@protonmail.couk","2"),
  ("Rowan Cain","1-856-373-2463","est.vitae@icloud.net","4");
INSERT INTO `staff` (`name`,`phone`,`email`,`role`)
VALUES
  ("Carolyn Dalton","(606) 537-8556","duis@icloud.couk","1"),
  ("Chantale Little","1-151-268-5159","nisi.aenean@hotmail.net","2"),
  ("Norman Hudson","1-488-270-6514","dictum.augue@hotmail.com","4"),
  ("Mariko Mccarthy","1-373-472-1232","fusce.mi@google.ca","4"),
  ("Mira Walters","(183) 364-6567","eget.tincidunt@aol.couk","2"),
  ("Margaret Levy","1-405-575-6322","bibendum.sed.est@outlook.org","3"),
  ("Dale Vang","1-614-658-4287","in.molestie@google.edu","1"),
  ("Stella Kramer","(376) 762-6104","auctor.quis@hotmail.ca","1"),
  ("Laith Marshall","(519) 684-7561","nunc.sed@google.com","2"),
  ("Robert Small","1-244-760-8341","nunc.sit@outlook.com","3");
INSERT INTO `staff` (`name`,`phone`,`email`,`role`)
VALUES
  ("Jelani Gutierrez","1-793-744-3639","mauris.sagittis.placerat@yahoo.net","4"),
  ("Dalton Cox","1-286-527-5995","ligula.aenean.euismod@outlook.org","3"),
  ("Shea Durham","1-382-835-3632","non@hotmail.net","3"),
  ("Lucius Hubbard","(745) 846-8371","montes.nascetur@yahoo.com","1"),
  ("Dora Greer","1-632-437-9813","dapibus.gravida@google.ca","1"),
  ("Phyllis Farrell","1-185-633-6018","vivamus.molestie.dapibus@google.net","2"),
  ("Dominic Pruitt","(884) 176-8243","nullam@hotmail.ca","3"),
  ("Jacob Erickson","(152) 752-5728","in.ornare@protonmail.org","2"),
  ("Shaine Baker","(538) 876-4134","metus.in@outlook.org","2"),
  ("Arsenio Berry","(688) 920-6457","pede.blandit@icloud.net","2");
INSERT INTO `staff` (`name`,`phone`,`email`,`role`)
VALUES
  ("Candice Beach","1-684-744-7636","augue.id@google.couk","4"),
  ("Ann Bradley","1-559-439-9800","per.inceptos.hymenaeos@yahoo.couk","1"),
  ("Iona Ford","(265) 855-2821","mi.lacinia@outlook.org","2"),
  ("August Vaughan","1-785-316-8202","lobortis.mauris@protonmail.com","2"),
  ("Dexter Schroeder","(426) 324-7536","nam@hotmail.edu","4"),
  ("Finn Cobb","(964) 113-5723","eu@icloud.net","4"),
  ("Destiny Michael","(493) 569-3203","primis.in@google.org","4"),
  ("Darrel Hall","(312) 732-3872","mattis.cras@outlook.org","3"),
  ("Kenneth Sexton","1-455-661-2337","id.libero@google.couk","1"),
  ("Calista Perry","1-814-676-1672","praesent.interdum@yahoo.net","2");
INSERT INTO `staff` (`name`,`phone`,`email`,`role`)
VALUES
  ("Steven Allen","(471) 332-8616","turpis.in@yahoo.net","2"),
  ("Xaviera Mcdonald","1-846-404-2511","morbi.non@outlook.couk","1"),
  ("Sage Webster","1-576-788-3841","non@hotmail.org","4"),
  ("Justina Hamilton","1-487-573-7262","at@aol.org","4"),
  ("Paloma Morin","1-880-236-7386","elementum@outlook.edu","1"),
  ("Fuller Luna","(453) 256-4776","magnis.dis.parturient@icloud.couk","1"),
  ("Phoebe Rosa","(172) 283-7444","auctor.odio@outlook.ca","1"),
  ("Hayley Kirkland","(964) 287-6447","laoreet@hotmail.couk","2"),
  ("Hadassah Lynn","(618) 971-2745","mus.aenean.eget@outlook.edu","3"),
  ("Lane Martin","1-979-453-2353","consequat.enim@hotmail.net","3");
INSERT INTO `staff` (`name`,`phone`,`email`,`role`)
VALUES
  ("Hiram Hahn","1-427-877-4311","arcu.ac@icloud.couk","1"),
  ("Stella O'donnell","1-942-255-4586","tempor.arcu.vestibulum@hotmail.org","3"),
  ("Liberty Mccormick","(757) 640-5316","tristique.senectus.et@google.ca","2"),
  ("Stacy Kerr","(716) 547-3822","risus.morbi@aol.ca","2"),
  ("Hall Whitley","(288) 636-6822","elementum.sem.vitae@hotmail.org","1"),
  ("Sydney Farley","1-360-103-5672","duis@aol.ca","2"),
  ("Carol Everett","(511) 731-2843","mauris.suspendisse.aliquet@outlook.org","4"),
  ("Elmo Merrill","(948) 247-4257","lacus@outlook.net","4"),
  ("Sage Sullivan","(528) 507-6553","massa.lobortis.ultrices@google.com","3"),
  ("Sigourney Justice","(710) 291-3312","vitae.aliquet.nec@outlook.com","1");
INSERT INTO `staff` (`name`,`phone`,`email`,`role`)
VALUES
  ("Harper Trevino","1-752-252-7482","massa.non@google.couk","2"),
  ("Aiko Lyons","1-447-587-4271","dui.nec.urna@icloud.ca","1"),
  ("Allistair Franklin","1-877-417-1165","arcu.eu@google.net","1"),
  ("Bo Martin","(946) 842-1568","porttitor.eros@aol.com","1"),
  ("Lane Marquez","1-687-707-7423","nibh@icloud.org","1"),
  ("Pandora Mayer","1-856-692-8834","a@hotmail.net","3"),
  ("Eagan Parsons","(557) 930-1157","nisl.maecenas@hotmail.org","4"),
  ("Burton Aguilar","1-747-761-5511","euismod.ac@protonmail.couk","2"),
  ("Macon Henry","(432) 336-9056","eu@yahoo.edu","3"),
  ("Lee Cain","(396) 652-1875","libero.mauris@icloud.edu","1");
INSERT INTO `staff` (`name`,`phone`,`email`,`role`)
VALUES
  ("Levi Thompson","(465) 292-9426","sed@icloud.net","2"),
  ("Phoebe Walker","1-546-668-8301","vitae.risus@icloud.com","2"),
  ("Wynne Bradford","(650) 976-2742","non.sapien@hotmail.couk","2"),
  ("Hanna Caldwell","(858) 311-7353","velit@hotmail.com","2"),
  ("Shad Robinson","1-648-677-8313","nonummy@icloud.com","2"),
  ("Wallace Schmidt","1-249-332-2032","aliquam@hotmail.org","2"),
  ("Ethan Parsons","1-582-269-9138","dolor.elit@aol.ca","1"),
  ("Garrison Bean","1-466-405-3122","dui.fusce@google.edu","2"),
  ("Macon Zamora","(854) 483-9947","fringilla.cursus@hotmail.couk","1"),
  ("Naomi Guerra","(336) 289-8328","maecenas@google.ca","1");
INSERT INTO `staff` (`name`,`phone`,`email`,`role`)
VALUES
  ("Chelsea Clements","1-424-543-6602","non@outlook.ca","3"),
  ("Teegan Collier","(577) 387-8648","bibendum.fermentum@yahoo.net","2"),
  ("Anika Armstrong","1-342-821-4124","ut.lacus@protonmail.couk","4"),
  ("Steel Decker","1-655-290-3430","consectetuer@yahoo.org","4"),
  ("Laura Terry","1-393-261-7643","ullamcorper@yahoo.org","2"),
  ("Katell Thompson","(694) 687-3728","nec.urna@google.org","4"),
  ("Nicholas Whitaker","1-655-335-2147","eget.ipsum@icloud.net","3"),
  ("Hanae Middleton","(486) 572-0294","aptent.taciti.sociosqu@outlook.com","3"),
  ("Devin Dennis","1-491-422-7915","tristique@google.couk","3"),
  ("George Bruce","(380) 470-7412","nulla.magna.malesuada@yahoo.net","3");
INSERT INTO `staff` (`name`,`phone`,`email`,`role`)
VALUES
  ("Elton Hess","1-494-153-6370","enim.mauris@outlook.ca","3"),
  ("Jackson Brennan","1-769-605-4273","vestibulum.ante.ipsum@protonmail.net","2"),
  ("Matthew Allen","1-481-645-6816","non.nisi@protonmail.com","2"),
  ("Roth Garrison","(222) 570-3414","orci.quis@google.edu","2"),
  ("Mona Griffin","(981) 836-5474","fermentum.metus.aenean@hotmail.net","3"),
  ("Bevis Herring","(557) 322-6523","rutrum.lorem@protonmail.com","2"),
  ("Lacy Mosley","1-528-458-9421","sed.molestie@protonmail.com","4"),
  ("Stephen Bradshaw","1-452-354-7823","eu.dolor@icloud.edu","3"),
  ("Moses Sparks","(855) 869-7513","vitae@icloud.net","1"),
  ("Austin Vasquez","(565) 375-1428","risus@protonmail.edu","2");
