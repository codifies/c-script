
MODNAME:=mod_cscript

# adjust these as per your distro
RESTART:=sudo sv restart apache
MODPATH:=/usr/libexec/httpd
LIBTOOL:=/usr/share/apr-1/build/libtool
INSTDSO:=/usr/share/apache/webroot/build/instdso.sh
INSTPATH:=/usr/libexec/httpd
INC:=-I/usr/include/apr-1 -I/usr/include/httpd -I/usr/include -I./include -I/usr/include/libxml2
# ?? MAP ? TODO
MAP:=/builddir/httpd-2.4.52
LIB:=-lxml2 -lcrypt -pthread -laprutil-1 -lapr-1



CC:=gcc
TCC:=-ltcc 




SRC:=$(wildcard src/*.c)
LO:=$(SRC:src/%.c=.build/%.lo)

install: .build/$(MODNAME).la
	sudo $(INSTDSO) SH_LIBTOOL='$(LIBTOOL) --silent' .build/$(MODNAME).la $(MODPATH) && $(RESTART)

.build/$(MODNAME).la: $(LO)	
	$(LIBTOOL) --tag=CC --silent --mode=link $(CC) $(LIB) -shared -Wl,-z,relro -Wl,-z,now -Wl,--as-needed -o .build/$(MODNAME).la  -rpath $(INSTPATH) -module -avoid-version  $(TCC) $(LO)

$(LO): .build/%.lo : src/%.c
	$(LIBTOOL) --silent --mode=compile cc -prefer-pic -fstack-clash-protection -D_FORTIFY_SOURCE=2 -mtune=generic -O2 -pipe -DAPR_IOVEC_DEFINED=1 -g -fdebug-prefix-map=$(MAP)=.build  -DLINUX -D_REENTRANT -D_GNU_SOURCE -pthread $(INC) $(INC) -c $< -o $@ && touch $(@:%.lo=%.slo) 

clean:
	sudo rm -rf .build


