
// testing relative path
#include "../ctest/cscript.h"

static char jsrc[] = "\n"
"var newnode = document.createTextNode('Embedded JavaScript works!');\n"
"document.getElementById('p2').appendChild(newnode);\n";

void main(void* body) 
{
    changeTitle("Second Cscript Example");
    void* p = addTag( body, "p", "*** Hello World this is Cscript ***");
    setAttrib(p, "style", "color:blue;");
    addTag(body,"p","This page demonstrates a more complex DOM structure"
                    " and embedded JavaScript");
    
    void* p2 = addTag( body, "p", 0);
    setAttrib(p2, "id", "p2");

    void* tab = addTag(body, "table", 0);
    
    void* tr = addTag(tab, "tr", 0);
    
    addTag(tr, "th", "Letters");
    
    void* th = addTag(tr, "th", 0);
    setTextContent(th,"Numbers");   // content can be set later

    tr = addTag(tab, "tr", 0); addTag(tr, "td", "A"); addTag(tr, "td", "1");
    tr = addTag(tab, "tr", 0); addTag(tr, "td", "B"); addTag(tr, "td", "2");
    tr = addTag(tab, "tr", 0); addTag(tr, "td", "C"); addTag(tr, "td", "3");

    void* script = addTag(body,"script", 0);  // puts src in CDATA
    setTextContent(script, jsrc); 
    // or
    //addTag(body,"script", jsrc);  // puts src in CDATA
    
    p = addTag(body, "p", 0); 
    void* a = addTag(p, "a", "index");
    setAttrib(a, "href", ".");
    
        
    #include "showsrc.h"
}
