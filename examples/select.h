
void showSelect(void* body, void* dbctx, char* sql) {
    
    char str[256];
    addTag(body, "hr", 0);

    addTag(body, "p", sql);
    void* res = selectDB(dbctx, sql);

    if (res) {
        int nr = getNumRowsDB(dbctx, res);
        sprintf(str, "number of rows %i", nr);
        addTag(body, "div", str);
        
        int nc = getNumColsDB(dbctx, res);
        sprintf(str, "number of cols %i", nc);
        addTag(body, "div", str);
        void* tab=addTag(body,"table",0);
        
        for (int i=1; i<nr+1; i++) {
            void* tr = addTag(tab,"tr",0);
            void* row = getRowDB(dbctx, res, i);
            if (row) {
                for (int j=0; j < nc; j++) {
                    char* val = getFieldDB(dbctx, row, j);
                    addTag(tr,"td",escapeHtmlStr(val));
                }
            }
        }
    } else {
        addTag(body, "p", "no result from query");
    }
    addTag(body, "hr", 0);
}

