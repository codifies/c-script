
#include "cscript.h"
#include <stdio.h>   // sprintf

#include "select.h"


void main(void* body) 
{
    changeTitle("Eighth Cscript Example");
    void* p = addTag( body, "p", "*** Hello World this is Cscript ***");
    setAttrib(p, "style", "color:blue;");
    addTag( body, "style",
                "table, td, th { border: 1px solid; padding: .25em; }"
                "table { border-collapse: collapse; }");
    addTag(body,"p","This page demonstrates mysql use");
    
    char* host = getENV("DB_HOST");
    char* name = getENV("DB_NAME"); 
    //char* user = "postgres";
    char* user = getENV("DB_USER"); 
    char* pass = getENV("DB_PASS");

    
    char str[1024];
    //sprintf(str, "postgresql://%s:%s@%s:5432/%s",user,pass,host,name);
    sprintf(str, "server=%s;uid=%s;pwd=%s;dbname=%s;",host,user,pass,name);
    void* a = addTag(body, "a", "index");
    setAttrib(a, "href", ".");
        

    //void* drv = initDB("pgsql");
    dbcontext* dbctx = initDB("mysql");
    
    openDB(dbctx, str);

    if (dbctx) {
        if (dbctx->handle) {  
            showSelect(body, dbctx, "select * from test;");
            showSelect(body, dbctx, "select * from sessions;");
        } else {
            addTag(body, "p", "could not open database");
        }
    } else {
        addTag(body, "p", "could not initialise db driver");
    }   
    
    
    #include "showsrc.h"
}
