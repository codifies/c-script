
/**
 * @file cscript.h
 * @brief Note about returned pointers \n
 * returned void pointers are often pointers to structures, however
 * they should be treated as opaque \n
 * while this drives a horse and cart through what little type safety 
 * is available in C it is done to avoid having to drag in all the 
 * headers for httpd, apr, apr-util etc \n
 * returned values should usually *NOT* be modified
 */

/** holder of DB variables passed as a pointer
*/
typedef struct dbcontext {
    /*@{*/
    void*           driver;         /**< abstraction driver */
    void*           handle;         /**< "handle" to the connection */
    /*@}*/
} dbcontext;

/**
 * Adds an XML tag to the page output \n
 * Warning if content contains unencoded XML entities it will silently
 * not add any content
 * @param parent    Parent tag to attach this tag to
 * @param name      Name of the tag ie a for <a> p for <p>
 * @param content   Textual content to go inside the tag
 * @return          pointer to newly created tag
 */ 
void* addTag(void* parent, char* name, char* content);

/**
 * Change the text of the title inside the head tag
 * @param title     text to use for the html title
 */
void changeTitle(char* title);

/**
 * Set an XML attribute, if the attribute already exists it will be replaced
 * @param node      pointer to the xml node
 * @param attribute name of the attribute to change
 * @param value     what you want the attribute to be set to
 */
void setAttrib(void* node, char* attribute, char* value);

/**
 * Set the textual content of a tag, use this to set a tags content
 * if it was not set when the tag was created or later need to be changed
 * @param node      pointer to the xml node
 * @param content   text to set the content with
 */
void setTextContent(void* node, char* content);

/**
 * Escapes a string - adding tags to special characters like &
 * @param s         string to escape
 * @return          an escaped version of the string
 */
char* escapeHtmlStr(char* s);

/**
 * Dumps the contents of the currently executed source to a string, only
 * really useful for the examples
 * @param s         pointer to a string, must contain sufficient memory
 * to hold the entire source code file.
 */
void dumpSrc(char* s);

/**
 * Gets the content of an apache environment variable
 * 
 * @param var       name of the variable to retrieve
 * @return          pointer to the value, should not be modified
 */
char* getENV(char* var);

/**
 * Gets the current time
 * @return          number of microseconds since 00:00:00 January 1, 1970 UTC 
 */
unsigned long now();

/**
 * Get the hostname of the request
 * @return      returns the hostname of the request
 */
char* getHost();

/**
 * Get the URI of the request
 * @return      return the unparsed URI of the request
 */
char* getURI();

/**
 * Is the current request a POST request
 * @return          returns 1 if the current request is a POST request
 */
int isPost();

/**
 * Is the connection via SSL
 * @return      returns 1 if the current request is over SSL
 */
int isSSL();

/**
 * Gets the value of a POST variable from a submitted form
 * @param varname   Name of the variable to 
 * @return          pointer to the value (should not be modified)
 */
const char* getPost(const char* varname);

/**
 * Retrieve and indexed post value \n
 * where a form contains variables named like var[0], var[1] etc
 * @param varname   Base name of the variable var[0..n] would be var
 * @param index     Index of the variable to retrieve
 * @return          Value of the variable
 */    
const char* getPostIdx(const char* varname, int index);

/**
 * Retrieve the value of a GET variable
 * @param varname   Name of the variable
 * @return          value of the variable
 */
char* getGet(char* varname);

/**
 * Sends a 302 redirect \n
 * The server redirects to the specified uri, not document content is output
 * @param uri       Address to redirect to
 */
void redirect(char* uri);

/**
 * Loads or creates a session, must be called before getSessionVal or 
 * setSessionVal
 * @return      pointer to use with session functions
 */
void* loadSession();

/**
 * Set a session value
 * @param sess      pointer to the session
 * @param key       name of the session variable
 * @param value     value to be stored under the key
 */
void setSessionVal(void* sess, char* key, char* value);

/**
 * retrieve a session value for a given key
 * @param sess              pointer to the session
 * @param key               name of the session variable
 * @param[in,out] value    pass the address of a pointer (&ptr), this pointer
 * will then point to the value of the session value
\code{.c}
    char str[256] = { 0 };
    char* v = NULL;
    getSessionVal(sess, "count", &v);
    if (v) {
        sprintf(str,"The value is : %s", v);
        addTag(body, "p", str);
    } else {
        addTag(body, "p", "No value stored or key doesn't exist");
    }
\endcode
 */
void getSessionVal(void* sess, char* key, char** value);

/**
 * Set a session value, the session is implicitly saved, however you can
 * use this to force save the session, for example after this is called
 * you can use dumpSess to see the URL encoded session.
 * @param sess      pointer to the session
 */
void saveSession(void * sess);

/**
 * Destroy a session
 * @param sess      Session to destroy
 */
void destroySession(void* sess);

/**
 * retrieve a session value for a given key
 * @param sess      pointer to the session
 * @return      the URL encoded version of the session    
 */
char* dumpSess(void* sess);

/**
 * initialise the database system
 * @param name      Driver name to use for example sqlite3, pgsql, mysql
 * @return      pointer to the driver
 */
void* initDB(char * name);

/**
 * open a database
 * @param dbctx       database driver to use
 * @param params     usually a database specific connection string
 * @return      database context
 */
void* openDB(void* dbctx, char *params);

/**
 * executes an SQL query that returns a result set (select for example)
 * @param dbctx       database driver to use
 * @param stm       query to execute
 * @return      returned result set
 */
void* selectDB(void* dbctx, char* stm);

/**
 * escapes a string for use in an SQL query

 * @param dbctx       database driver to use
 * @param string        string to escape
 * @return      escaped string
 */
char* escapeDB(void* dbctx, char* string);

/**
 * sets the database to use, this needs to be done for prepared
 * statements
 * 
 * @param dbctx       database driver to use
 * @param name      name of the database
 */ 
void setNameDB(void* dbctx, char * name );

/**
 * executes an SQL query that does *not* return a result set (insert/delete for example)
 * @param dbctx       database driver to use
 * @param stm       query to execute
 * @return      number of rows effected
 */
int queryDB(void* dbctx, char* stm);

/**
 * Returns the number of rows in a resultset
 * @param dbctx       database driver to use
 * @param res       resultset to get the number of rows of
 * @return      the number of rows in the resultset
 */
int getNumRowsDB(void* dbctx, void* res);

/**
 * Returns the number of columns in a resultset
 * @param dbctx       database driver to use
 * @param res       resultset to get the number of columns of
 * @return      the number of columns in the resultset
 */
int getNumColsDB(void* dbctx, void* res);

/**
 * gets a specific row from a resultset
 * @param dbctx       database driver to use
 * @param res       resultset to get the row from
 * @param rn        index of the row to retrieve
 * @return      the nth row of the result set
 */
void* getRowDB(void* dbctx, void* res, int rn);

/**
 * get the value of a field from a row
 * @param dbctx       database driver to use
 * @param row       row to get the field from
 * @param col       index of the column to retrieve
 * @return      value of the field
 */
char* getFieldDB(void* dbctx, void* row, int col);

/**
 * prepares a parametised SQL statement for use \n
 * setNameDB needs to be called beforhand regardless of connection string \n
 * Here is the full list of format specifiers that this function understands
 * and what they map to in SQL: %hhd (TINY INT), %hhu (UNSIGNED TINY INT), 
 * %hd (SHORT), %hu (UNSIGNED SHORT), %d (INT), %u (UNSIGNED INT), %ld (LONG), 
 * %lu (UNSIGNED LONG), %lld (LONG LONG), %llu (UNSIGNED LONG LONG), 
 * %f (FLOAT, REAL), %lf (DOUBLE PRECISION), %s (VARCHAR), %pDt (TEXT), 
 * %pDi (TIME), %pDd (DATE), %pDa (DATETIME), %pDs (TIMESTAMP), 
 * %pDz (TIMESTAMP WITH TIME ZONE), %pDb (BLOB), %pDc (CLOB) and 
 * %pDn (NULL). 
 * Not all databases have support for all these types, so the underlying 
 * driver will attempt the "best match" where possible. A % followed by 
 * any letter not in the above list will be interpreted as VARCHAR (i.e. %s). 
 * @param dbctx       database driver to use
 * @param query     statement to prepare
 * @return      value of the field
 */
void* prepareDB(void* dbctx, char* query);

/** 
 * Executes a prepared statement that does not return a rowset
 * @param dbctx       database driver to use
 * @param stm       statement to use
 * @param args      see pselectDB for agument specification
 * @return      number of rows affected
 */
int pqueryDB(void* dbctx, void* stm, char** args);

/** 
 * Executes a prepared statement that returns a resultset
 * @param dbctx       database driver to use
 * @param stm       statement to use
 * @param args      the arguments (see code example below)
 * @return      returned result
\code{.c}
char* sql = "select * from test where Field1 > %i and Field3 > %i;";
stm = prepareDB(drv, hdl, sql);
char* arg[3] = { "2", "1", NULL };
if (stm) {
    void* res = pselectDB(drv,hdl,stm,arg);
    ...
\endcode
*/
void* pselectDB(void* dbctx, void* stm, char** args);

/**
 * close a previously opened database connection
 * @param dbctx       database driver to use
 */
void closeDB(void* dbctx);

/**
 * create a one way hash for password checking if check is null
 * if check is not null then it should be reference hash to check
 * against the supplied passphrase
 * @param phrase    a passphrase
 * @param check     null to create a hash or a hash to check against
 * @return          a generated hash
 */
char* dohash(char* phrase, char* check); 
