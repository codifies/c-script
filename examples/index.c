
#include "cscript.h"

void main(void* body) 
{
    changeTitle("Cscript Index");
    addTag(body, "p", "*** Hello World this is Cscript ***");
    addTag(body, "p", "Index page");
    
    void* p = addTag(body, "p", 0);
    void* a = addTag(p, "a", "Example 1 - Basic DOM creation");
    setAttrib(a, "href", "test1.c");
    
    p = addTag(body, "p", 0); a = addTag(p, "a", "Example 2 - Embedded JavaScript");
    setAttrib(a, "href", "test2.c");
    
    p = addTag(body, "p", 0); a = addTag(p, "a", "Example 3 - Form handling");
    setAttrib(a, "href", "test3.c");
    
    p = addTag(body, "p", 0); a = addTag(p, "a", "Example 4 - Using sessions");
    setAttrib(a, "href", "test4.c");
    
    p = addTag(body, "p", 0); a = addTag(p, "a", "Example 5 - acessing databases");
    setAttrib(a, "href", "test5.c");
    
    p = addTag(body, "p", 0); a = addTag(p, "a", "Example 6 - prepared database statements");
    setAttrib(a, "href", "test6.c");    
    
    p = addTag(body, "p", 0); a = addTag(p, "a", "Example 7 - GET request");
    setAttrib(a, "href", "test7.c?a=1&b=2");    
    
    p = addTag(body, "p", 0); a = addTag(p, "a", "Example 8 - using PostgreSQL or mysql");
    setAttrib(a, "href", "test8.c");        
    
    p = addTag(body, "p", 0); a = addTag(p, "a", "Example 8a - using PostgreSQL or mysql prepared statement");
    setAttrib(a, "href", "test8a.c");        
    
    p = addTag(body, "p", 0); a = addTag(p, "a", "Example 9 - redirect (to here - or not randomly) ");
    setAttrib(a, "href", "test9.c");   
    
    p = addTag(body, "p", 0); a = addTag(p, "a", "Example 10 - hash creation and checking ");
    setAttrib(a, "href", "test10.c");   
    
    p = addTag(body, "p", 0); a = addTag(p, "a", "Test app ");
    setAttrib(a, "href", "testapp/");   
}
