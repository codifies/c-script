
#include "cscript.h"
#include <stdio.h>   // sprintf

#include "select.h"

void main(void* body) 
{
    changeTitle("fifth Cscript Example");
    addTag( body, "style",
        "table, td, th { border: 1px solid; padding: .25em; }"
        "table { border-collapse: collapse; }");
        
    addTag( body, "p", "*** Hello World this is Cscript ***");
    addTag( body, "p", "This page demonstrates database access");

    void* p = addTag(body, "p", 0); 
    void* a = addTag(p, "a", "index");
    setAttrib(a, "href", ".");

    dbcontext* dbctx = initDB("sqlite3");

    if (dbctx) {
        openDB(dbctx, "/tmp/test.db");
        if (dbctx->handle) {  
            showSelect(body, dbctx, "select * from test");
        } else {
            addTag(body, "p", "could not open database");
        }
        addTag(body, "br", 0);

        openDB(dbctx, "/tmp/session.db");
        if (dbctx->handle) {
            showSelect(body, dbctx, "select * from session");
        } else {
            addTag(body, "p", "could not open db");
        }
    } else {
        addTag(body, "p", "could not initialise db driver");
    }
       
    int nr = 0;
    char str[256];
    openDB(dbctx, "/tmp/test.db");
    if (dbctx->handle) {
        nr = queryDB(dbctx, "INSERT INTO test (field2) VALUES ('insert1');");
        nr += queryDB(dbctx, "INSERT INTO test (field2) VALUES ('insert2');");
        addTag(body, "br", 0); addTag(body, "br", 0); 
        addTag(body, "div", "INSERT INTO test (field2) VALUES ('insert1');"); 
        addTag(body, "div", "INSERT INTO test (field2) VALUES ('insert2');"); 
        addTag(body, "br", 0); 
        sprintf(str, "total rows affected %i", nr);
        addTag(body, "div", str);
        showSelect(body, dbctx, "select * from test");
        
        addTag(body, "br", 0); addTag(body, "p", "DELETE FROM test WHERE Field2 LIKE 'insert%'"); 
        nr = queryDB(dbctx, "DELETE FROM test WHERE Field2 LIKE 'insert%'");
        sprintf(str, "rows affected %i", nr);
        addTag(body, "div", str);
        showSelect(body, dbctx, "select * from test");  
    } else {
        addTag(body, "p", "could not open test.db");
    }  
     
 
          
    #include "showsrc.h"
}
