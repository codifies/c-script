
#include "cscript.h"
#include <stdio.h>
#include <string.h>

void doInput(char* name, char* value, char* label, void* parent) {
    addTag(parent, "div", label);
    void* inp = addTag(parent, "input", 0);
    setAttrib(inp, "type", "text"); 
    setAttrib(inp, "name", name);
    setAttrib(inp, "value", value);
}

void main(void* body) {
    changeTitle("Tenth Cscript Example");
    void* p = addTag( body, "p", "*** Hello World this is Cscript ***");
    setAttrib(p, "style", "color:blue;");
    addTag(body,"p","This page demonstrates hash checking");
    
    void* frm = addTag(body, "form",0);
    setAttrib(frm, "method", "post"); setAttrib(frm, "name", "form1");

    char phrase[128], check[128], hash[128], str[256];
    sprintf(phrase, "%s", (char*)getPost("refPhrase")); 
    doInput("refPhrase", phrase, "Reference phrase", frm);     

    sprintf(check, "%s", (char*)getPost("chkPhrase")); 
    doInput("chkPhrase", check, "Check phrase", frm);       

    void* div = addTag(frm, "div", 0);
    void* submit = addTag(div, "input", 0);
    setAttrib(submit, "type", "submit");
    setAttrib(submit, "value", "Post");
    
    char* tmp = dohash(phrase, 0);
    strcpy(hash, tmp);

    if (strlen(phrase)) {
        sprintf(str, "reference hash: %s", hash);
        addTag(body, "pre", str);
        if (strlen(check)) {
            char* tmp = dohash(check, hash);
            if (strcmp(tmp, hash)) {
                addTag(body, "p", "You shall not pass"); 
            } else {
                addTag(body, "p", "reference hash and check phrase match"); 
            } 
        }
    }
    
    p = addTag(body, "p", 0); 
    void* a = addTag(p, "a", "index");
    setAttrib(a, "href", ".");
    
    #include "showsrc.h"
}
