
#include "cscript.h"

void main(void* body) {
    changeTitle("Third Cscript Example");
    void* p = addTag( body, "p", "*** Hello World this is Cscript ***");
    setAttrib(p, "style", "color:blue;");
    addTag(body,"p","This page demonstrates using a form");
    
    if (isPost()) {
        addTag(body,"p","POST request");
    } else {
        addTag(body,"p","GET request");
    }
    
    p = addTag(body, "p", 0);
    addTag(p,"span","last post input1: ");
    addTag(p,"span",(char*)getPost("input1"));
    
    void* frm = addTag(body, "form",0);
    setAttrib(frm, "onsubmit", "return confirm('Do you really want to submit the form?');");
    setAttrib(frm, "method", "post");
    setAttrib(frm, "name", "form1");
    
    void* text = addTag(frm, "input", 0);
    setAttrib(text, "type", "text");
    setAttrib(text, "name", "input1");
    setAttrib(text, "value", (char*)getPost("input1"));
        
    // an array of form variables
    char* str;
    char* name="other[0]";
    for (int i=0; i<3; i++) { 
        str = (char*)getPostIdx("other", i);
        void* imp = addTag(frm, "input", 0);
        name[6] = '0'+i;
        setAttrib(imp, "name", name);
        setAttrib(imp, "value", str);
    }

    void* submit = addTag(frm, "input", 0);
    setAttrib(submit, "type", "submit");
    setAttrib(submit, "value", "Post");
    
    p = addTag(body, "p", 0); 
    void* a = addTag(p, "a", "index");
    setAttrib(a, "href", ".");
    
        
    #include "showsrc.h"
}
