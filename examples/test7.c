
#include "cscript.h"
#include <stdio.h>   // sprintf

void dumpVar(void* parent, char* var) {
    char msg[1024];
    char *val = getGet(var);
    sprintf(msg,"var %s = %s", var, escapeHtmlStr(val));
    addTag(parent, "div", msg);        
    void* text = addTag(parent, "input", 0);
    setAttrib(text, "type", "text");
    setAttrib(text, "name", var);
    setAttrib(text, "value", val);
}

void main(void* body) 
{
    changeTitle("Seventh Cscript Example");
    void* p = addTag( body, "p", "*** Hello World this is Cscript ***");
    setAttrib(p, "style", "color:blue;");
    addTag(body,"p","This page demonstrates a GET request");
    
    p = addTag(body, "p", 0); 
    void* a = addTag(p, "a", "index");
    setAttrib(a, "href", ".");
    
    void* frm = addTag(body, "form",0);
    setAttrib(frm, "method", "get");
    setAttrib(frm, "name", "form1");
    
    dumpVar(frm, "a");
    dumpVar(frm, "b");
    
    void* submit = addTag(frm, "input", 0);
    setAttrib(submit, "type", "submit");
    setAttrib(submit, "value", "Post");
    
    
    #include "showsrc.h"
}
