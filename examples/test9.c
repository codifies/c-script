
#include "cscript.h"
#include <stdlib.h> // rand
    
void main(void* body) 
{
    if ( rand() & 1 ) { // 50/50 (ish) chance
        // you could add content tags then decide later to redirect
        // but nothing will be output
        addTag( body, "p", "Won't be seen");
        redirect("index.c");
    } else {
        // added the random chance so the example wasn't a 1 line function!
        changeTitle("Ninth Cscript Example");
        void* p = addTag( body, "p", "*** Hello World this is Cscript ***");
        setAttrib(p, "style", "color:blue;");
        addTag(body,"p","Randomly decided not to redirect!");
        
        p = addTag(body, "p", 0); 
        void* a = addTag(p, "a", "index");
        setAttrib(a, "href", ".");   
        
        #include "showsrc.h"     
    }
}
