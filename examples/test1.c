
#include "cscript.h"

void main(void* body) 
{
    changeTitle("First Cscript Example");
    void* p = addTag( body, "p", "*** Hello World this is Cscript ***");
    setAttrib(p, "style", "color:blue;");
    addTag(body,"p","This page demonstrates using a DOM to create content");

    p = addTag(body, "p", 0); 
    void* a = addTag(p, "a", "index");
    setAttrib(a, "href", ".");
    
    #include "showsrc.h"
}
