
#include "../cscript.h"
#include "util.h"

void main(void* body) 
{
    char* uid = checklogin();
    
    changeTitle("Cscript Test App");
    addTag( body, "style",
        "table, td, th { border: 1px solid; padding: .25em; }"
        "table { border-collapse: collapse; }");
    
    void* p = addTag(body,"p","This is the test app");
    setAttrib(p, "style", "color:blue;");
    
    char str[1024];
    char* uname;
    // session already started by checklogin
    getSessionVal(sess,"uname",&uname);
    sprintf(str,"Hello %s",uname);
    addTag(body,"p",str);
    addTag(body,"br",0);
    
    void* a = addTag(body, "a", "Log Out");
    setAttrib(a, "href", "logout.c");
    a = addTag(body, "a", "Change Password");
    setAttrib(a, "href", "chpasswd.c");
    
    addTag(body, "br", 0);
    addTag(body, "br", 0);
    
    int page = 1;
    if (isPost()) { // get page number from POST
        char* pg = (char*)getPost("page");
        if (pg[0]) page = atoi(pg);
    }
        
    dbcontext* dbctx = initDB("mysql");
    if (dbctx) {
        sprintf(str, "server=%s;uid=%s;pwd=%s;dbname=%s;",
            getENV("DB_HOST"),
            getENV("DB_USER"),
            getENV("DB_PASS"),
            getENV("DB_NAME"));
        openDB(dbctx, str);
        if (dbctx->handle) {
            //char sql[1024];
            //sprintf(sql,"%s", "select * from staff order by id");
            
            // can't be terminated with ;
            char* sql = "select * from staff order by id";
            void* res = getPage(dbctx, sql, page, 12, body);            
            showRes(body, dbctx, res);
        }
    }
}
