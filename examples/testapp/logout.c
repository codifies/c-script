
#include "../cscript.h"
#include "util.h"

void main(void* body) 
{
    void* sess = loadSession();
    destroySession(sess);
    redirectRel("../index.c");
}
