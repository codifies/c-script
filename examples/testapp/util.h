
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

// redirect to an address relative to current url
void redirectRel(char* rel) 
{
    char str[1024] = {0};
    char url[1024] = {0};
    sprintf(str,"https://%s%s",getHost(),getURI());
    char* p = str + strlen(str);
    while (p[0]!='/') p--;
    p[0] = 0;
    sprintf(url,"%s/%s",str,rel);
    redirect(escapeHtmlStr(url));
}

void showRes(void* parent, void* dbctx, void* res) 
{
    if (dbctx) {
        int nr = getNumRowsDB(dbctx, res);
        int nc = getNumColsDB(dbctx, res);
        void* tab=addTag(parent,"table",0);
        
        for (int i=1; i<nr+1; i++) {
            void* tr = addTag(tab,"tr",0);
            void* row = getRowDB(dbctx, res, i);
            if (row) {
                for (int j=0; j < nc; j++) {
                    char* val = getFieldDB(dbctx, row, j);
                    addTag(tr,"td",escapeHtmlStr(val));
                }
            }
        }
    } else {
        addTag(parent, "p", "no resultset");
    }
}

void* getPage(dbcontext* dbctx, char* query, int page, int rpp, void* parent)
{
    char str[1024];
    int tp = 0; // total pages
    
    sprintf(str,"with _CTE_COUNT as (%s) select count(*) from _CTE_COUNT", query);
    
    void* res = selectDB(dbctx, str);

    if (res){
        int nr = getNumRowsDB(dbctx, res);
        if (nr==1) {
            void* row = getRowDB(dbctx, res, 1);
            if (row) {
                tp = ceilf(atof(getFieldDB(dbctx, row, 0))/rpp);
            }
        }
    }
    
    for (int i=1; i < tp+1; i++) {
        void* div = addTag(parent, "div", 0);
        setAttrib(div,"style","display:inline-block;");
        void* frm = addTag(div, "form", 0);
        setAttrib(frm, "method", "post");
        void* h = addTag(frm, "input", 0);
        sprintf(str,"%i", i);
        setAttrib(h, "name", "page");
        setAttrib(h, "value", str);
        setAttrib(h, "type", "hidden");
        void* inp = addTag(frm, "input", 0);
        setAttrib(inp, "type", "submit");
        setAttrib(inp, "value", str);
        if (page==i) setAttrib(inp, "disabled", 0);
    }

    int actPage = (page - 1) * rpp;
    sprintf(str, "%s LIMIT %i, %i", query, actPage, rpp);
    
    return selectDB(dbctx, str);
} 

void* sess = NULL; // global reference to the session

char* checklogin() 
{
    char str[1024];
 

    if (!isSSL()) {
        // redirect to SSL - not just a nice to have...
        sprintf(str,"https://%s%s",getHost(),getURI());
        redirect(escapeHtmlStr(str));
        return 0;
    }

    sess = loadSession();
    
    char* uid = NULL;  
    char* zero = "0";
    getSessionVal(sess, "uid", &uid);
    // session variable uid is set by successful login
    if (!uid) {
        redirectRel("login.c");
        return 0;
    }
    
    return uid;
    
}

// add an item to a form
void formItem(void* parent, char* name, char* type, char* value) 
{
    void* tag = addTag(parent, "input", 0);
    setAttrib(tag, "type", type);
    setAttrib(tag, "name", name);
    if (value[0]) setAttrib(tag, "value", value);    
}

