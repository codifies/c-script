
#include "../cscript.h"
#include "util.h"

void main(void* body) 
{
    if (isPost()) {
        void* sess = loadSession();

        char str[1024];
        sprintf(str, "server=%s;uid=%s;pwd=%s;dbname=%s;",
                        getENV("DB_HOST"),
                        getENV("DB_USER"),
                        getENV("DB_PASS"),
                        getENV("DB_NAME"));
        
        dbcontext* dbctx = initDB("mysql");
        
        if (dbctx) {
            openDB(dbctx, str);
            if (dbctx->handle) {

                char refhash[1024] = {0};
                char uname[1024] = {0};
                char uid[1024] = {0};
                char sql[1024] = {0};
                
                // escape user input to avoid injection
                sprintf(str,"select * from users where username = '%s'", escapeDB(dbctx,(char*)getPost("user")));
                void* res = selectDB(dbctx, str);
                
                // if the entered user name is in the DB
                // get its UID and password reference hash
                if (res){
                    int nr = getNumRowsDB(dbctx, res);
                    if (nr==1) {
                        void* row = getRowDB(dbctx, res, 1);
                        if (row) {
                            char* val = getFieldDB(dbctx, row, 0);
                            strcpy(uid,val);
                            val = getFieldDB(dbctx, row, 1);
                            strcpy(uname,val);
                            val = getFieldDB(dbctx, row, 2);
                            strcpy(refhash,val);
                        }
                    }
                }

                char* passin;
                passin = (char*)getPost("pass");

                if (strlen(passin)) {
                    // check if the two hashes are equivalent
                    if (strlen(refhash)) {
                        char* tmp = dohash(passin, refhash);
                        if (strcmp(tmp, refhash)==0) {
                            // set the sessions uid so checklogin
                            // knows the login is valid
                            setSessionVal(sess, "uid", uid);
                            setSessionVal(sess, "uname", uname);

                            // allow the user to access the app
                            redirectRel("index.c");
                            return;
                        } 
                    }
                }
            // fail silently if the password doesn't match or can't find the user    
            } else {
                addTag(body, "p", "could not open database");
            }
        } else {
            addTag(body, "p", "could not initialise db driver");
        }  

    }
    
    // simple form to get the user name and password
    changeTitle("LOGIN Test App");
    addTag(body, "p", "Please login");

    void* frm = addTag(body, "form",0);
    setAttrib(frm, "method", "post");
    setAttrib(frm, "name", "formLogin");
    
    formItem(frm, "user", "text", "");

    addTag(frm,"br",0); addTag(frm,"br",0);

    formItem(frm, "pass", "password", "");

    addTag(frm,"br",0); addTag(frm,"br",0);

    formItem(frm, "submit", "submit", "Log in");
}
