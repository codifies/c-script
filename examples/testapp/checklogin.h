

char* checklogin() 
{
    char str[1024];
    /*
    if (!isSSL()) {
        // redirect to SSL - not just a nice to have...
        sprintf(str,"https://%s%s",getHost(),getURI());
        redirect(escapeHtmlStr(str));
        return 0;
    }
    */
    
    void* sess = loadSession();
    
    char* uid = NULL;  
    char* zero = "0";
    getSessionVal(sess, "uid", &uid);
    if (!uid) {
        sprintf(str,"https://%s%s",getHost(),getURI());
        char* p = str + strlen(str);
        while (p[0]!='/') p--;
        p[0] = 0;
        sprintf(str,"%s/login.c",str);
        redirect(escapeHtmlStr(str));
        return 0;
    }
    
    return uid;
    
}
