
#include "../cscript.h"
#include "util.h"

void main(void* body) 
{
    char* uid = checklogin();
    char str[1024];
    char* uname;
    
    getSessionVal(sess, "uname", &uname);
    char* arg[3] = { uname, 0, 0 };
    

                
    if (isPost()) {
        sprintf(str, "server=%s;uid=%s;pwd=%s;dbname=%s;",
                        getENV("DB_HOST"),
                        getENV("DB_USER"),
                        getENV("DB_PASS"),
                        getENV("DB_NAME"));
        
        dbcontext* dbctx = initDB("mysql");
        
        if (dbctx) {
    
            openDB(dbctx, str);
            if (dbctx->handle) {
                
                char refhash[1024] = {0};
                
                sprintf(str,"select * from users where username = '%s'", uname);
                void* res = selectDB(dbctx, str);
                addTag(body, "p", str);
                
                // if the entered user name is in the DB
                // get its UID and password reference hash
                if (res){
                    int nr = getNumRowsDB(dbctx, res);
                    if (nr==1) {
                        void* row = getRowDB(dbctx, res, 1);
                        if (row) {
                            char* val = getFieldDB(dbctx, row, 2);
                            strcpy(refhash,val);
                        }
                    } 
                    sprintf(str, "number of rows from user query %i",nr);
                    addTag(body, "p", str); 
                } else {
                    addTag(body, "p", "no result from user query");
                }

                char* passin;
                char* pass1;
                char* pass2;
                passin = (char*)getPost("pass");
                pass1 = (char*)getPost("newpass1");
                pass2 = (char*)getPost("newpass2");
                
                // check pass1 and pass2 are the same
                
                if (strlen(pass1)>5) {

                    if (strcmp(pass1,pass2)==0) {
                        if (strlen(passin)) {
                            // check if the two hashes are equivalent
                            if (strlen(refhash)) {
                                char* tmp = dohash(passin, refhash);
                                
                                if (strcmp(tmp, refhash)==0) {
                                    // finally change password 
                                    /* refhash and username don't need to be escaped */
                                    sprintf(str,"update users set pass = '%s' where username = '%s' ", 
                                                dohash(pass1,0),
                                                uname                           
                                            );
                                    queryDB(dbctx, str);

                                    redirectRel("logout.c");

                                    return;
                                } else {
                                    addTag(body,"div","please try again");
                                }
                            }
                        } else {
                            addTag(body,"div","Please enter your current password");
                        }

                    } else {
                        addTag(body,"div","repeated passwords must match");
                    }
                } else {
                    addTag(body,"div","New password too short");
                } 
            } else {
                addTag(body, "p", "could not open database");
            }
        } else {
            addTag(body, "p", "could not initialise db driver");
        }  

    } else {
        addTag(body,"div","not POST");
    }
    
    
    sprintf(str,"user: %s ", uname);
    addTag(body,"div",str);
    
    // simple form to get the user name and password
    changeTitle("Change Password Test App");
    
    //void formItem(void* parent, char* name, char* type, char* value) 

    void* frm = addTag(body, "form",0);
    setAttrib(frm, "method", "post");
    setAttrib(frm, "name", "formLogin");
    
    void* d = addTag(frm,"div","current password:");
    setAttrib(d, "style", "display:inline;");
    formItem(frm, "pass", "password", "");
    addTag(frm,"br",0); addTag(frm,"br",0);

    d = addTag(frm,"div","new password:");
    setAttrib(d, "style", "display:inline;");
    formItem(frm, "newpass1", "password", "");
    addTag(frm,"br",0); addTag(frm,"br",0);

    d = addTag(frm,"div","repeat password:");
    setAttrib(d, "style", "display:inline;");
    formItem(frm, "newpass2", "password", "");
    addTag(frm,"br",0); addTag(frm,"br",0);

    formItem(frm, "submit", "submit", "Change Password");
}
