
#include "cscript.h"
#include <stdio.h>   // sprintf



void main(void* body) {
    changeTitle("sixth Cscript Example");
    addTag( body, "p", "*** Hello World this is Cscript ***");
    addTag( body, "p", "This page demonstrates prepared db statement");
    
    void* stm = NULL;      // prepared statement
    dbcontext* dbctx = NULL;      // driver
    char str[256];
    
    dbctx = initDB("sqlite3");

    if (dbctx) {
        
        openDB(dbctx, "/tmp/test.db");
        if (dbctx->handle) {
            
            char* sql = "select * from test where Field1 > %i and Field3 > %i;";
            stm = prepareDB(dbctx, sql);
            char* arg[3] = { "2", "1", NULL };
            
            if (stm) {
                
                void* res = pselectDB(dbctx,stm,arg);
                
                if (res) {
                    
                    int nr = getNumRowsDB(dbctx, res);
                    for (int i=1; i<nr+1; i++) {
                        void* row = getRowDB(dbctx, res, i);
                        if (row) {
                            char* f1 = getFieldDB(dbctx, row, 0);
                            char* f2 = getFieldDB(dbctx, row, 1);
                            char* f3 = getFieldDB(dbctx, row, 2);
                            sprintf(str, "%s, %s, %s", escapeHtmlStr(f1), escapeHtmlStr(f2), escapeHtmlStr(f3));
                            addTag(body, "div", str);
                        } else {
                            sprintf(str, "error getting row %i", i);
                            addTag(body, "div", str);
                        }
                    }
                } else {
                    addTag(body, "div", "select from statement failed"); 
                }
                
            } else {
                addTag(body, "div", "statement creation failed"); 
            }
            
        } else {
            addTag(body, "div", "Can't open the database");            
        }
        
    } else {
        addTag(body, "div", "Can't open the db driver");
    }
    
    void* a = addTag(body, "a", "index");
    setAttrib(a, "href", ".");
    
    #include "showsrc.h"
    
}
