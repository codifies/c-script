
#include "cscript.h"
#include <stdio.h>   // sprintf

#include "select.h"

void showRes(void* parent, dbcontext* ctx, void* res) 
{
    if (res) {
        char str[1024];
        int nr = getNumRowsDB(ctx, res);
        int nc = getNumColsDB(ctx, res);
        sprintf(str,"columns %i, rows %i",nc,nr);
        addTag(parent,"p",str);
        void* tab=addTag(parent,"table",0);
        
        for (int i=1; i<nr+1; i++) {
            void* tr = addTag(tab,"tr",0);
            void* row = getRowDB(ctx, res, i);
            if (row) {
                for (int j=0; j < nc; j++) {
                    char* val = getFieldDB(ctx, row, j);
                    addTag(tr,"td",escapeHtmlStr(val));
                }
            }
        }
    } else {
        addTag(parent, "p", "no resultset");
    }
}

void main(void* body) 
{
    changeTitle("Eighth Cscript Example");
    void* p = addTag( body, "p", "*** Hello World this is Cscript ***");
    setAttrib(p, "style", "color:blue;");
    addTag( body, "style",
                "table, td, th { border: 1px solid; padding: .25em; }"
                "table { border-collapse: collapse; }");
    addTag(body,"p","This page demonstrates mysql and prepared statements");
    
    char* host = getENV("DB_HOST");
    char* name = getENV("DB_NAME"); 
    //char* user = "postgres";
    char* user = getENV("DB_USER"); 
    char* pass = getENV("DB_PASS");

    
    char str[1024];
    //sprintf(str, "postgresql://%s:%s@%s:5432/%s",user,pass,host,name);
    sprintf(str, "server=%s;uid=%s;pwd=%s;dbname=%s;",host,user,pass,name);
    void* a = addTag(body, "a", "index");
    setAttrib(a, "href", ".");
        
    //dbcontext* dbctx = initDB("pgsql");
    dbcontext* dbctx = initDB("mysql");

    if (dbctx) {
        openDB(dbctx, str);
        if (dbctx->handle) {  
            
            char* sqls="select * from users where username = %s";
            void* stm = NULL;
            
            stm = prepareDB(dbctx, sqls);
            
            char* arg[2] = { "fred", 0 };

            
            void* res = NULL;
            if (stm) {
                addTag(body,"p","statement prepared");
                res = pselectDB(dbctx,stm,arg);
                showRes(body, dbctx, res);
            } else {
                addTag(body, "p", "could not prepare statement");
            }
        } else {
            addTag(body, "p", "could not open database");
        }
    } else {
        addTag(body, "p", "could not initialise db driver");
    }   
    
    
    #include "showsrc.h"
}
