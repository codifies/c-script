
#include "cscript.h"
#include <stdlib.h>  // atoi
#include <stdio.h>   // sprintf
#include <string.h> // strlen
char str[1024];

void main(void* body) {
    if (!isSSL()) {
        // redirect to SSL - not just a nice to have...
        sprintf(str,"https://%s%s",getHost(),getURI());
        redirect(escapeHtmlStr(str));
    }
    changeTitle("forth Cscript Example");
    addTag( body, "p", "*** Hello World this is Cscript ***");
    addTag( body, "p", "This page demonstrates simple sessions");
    
    void* a = addTag(body, "a", "again");
    setAttrib(a, "href", "test4.c");
    void* ad = addTag(body, "a", "destroy");
    setAttrib(ad, "href", "test4.c?destroy=1");
        
    void* sess = loadSession();
    
    if (getGet("destroy")[0]) {
        destroySession(sess);
        sprintf(str,"https://%s%s",getHost(),getURI());
        char* q = strstr(str, "?");
        if (q) q[0]=0;
        redirect(escapeHtmlStr(str));
        return;
    } 

    char* v = NULL;  
    char* zero = "0";
    getSessionVal(sess, "count", &v);

    if (v) {
        addTag(body, "p", "v set");
    } else {
        addTag(body, "p", "v not set");
        v = zero;
    }
    int vi = atoi(v);
    
    vi++;
    sprintf(str, "%i", vi);
    setSessionVal(sess, "count", str);   
    
    addTag(body, "p", str);
    
    setSessionVal(sess, "otherval", "other"); // testing multiple session variables
        
    void* p = addTag(body, "p", 0); 
    a = addTag(p, "a", "index");
    setAttrib(a, "href", ".");

    
    #include "showsrc.h"
}
