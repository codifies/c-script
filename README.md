# ![](./logo.png) C-Script

## mod_cscript

An Apache module to serve C source as a scrpting language for the web.    

This is pre-production software (Release early, release often)
[![License: MIT](LicenseMIT.svg)](https://opensource.org/licenses/MIT)

Author C.Camacho (codifies)


## Configuring Apache

Here is an example configuration for apache.

```
LoadModule mod_cscript /usr/libexec/httpd/mod_cscript.so
AddHandler cscript_handler .c

<Directory "/srv/www/apache/ctest">
    DirectoryIndex index.c
    Options Indexes FollowSymLinks
    AllowOverride None
    Require all granted
    Session On

    # disallow .h
    <FilesMatch "\.(h)$">
        Order allow,deny
        Deny from all
    </FilesMatch>

</Directory>
DBDriver mysql
DBDParams "server=localhost;uid=root;pwd=<PASSWORD>;dbname=test;"

DBDPrepareSQL "delete from sessions where sesskey = %s" deletesession
DBDPrepareSQL "update sessions set value = %s, expiry = %lld, sesskey = %s where sesskey = %s" updatesession
DBDPrepareSQL "insert into sessions (value, expiry, sesskey) values (%s, %lld, %s)" insertsession
DBDPrepareSQL "select value from sessions where sesskey = %s and (expiry = 0 or expiry > %lld)" selectsession
DBDPrepareSQL "delete from sessions where expiry != 0 and expiry < %lld" cleansession

SessionDBDCookieName session path=/;SameSite=Lax;

# time out session in 15mins if no activity
SessionMaxAge 900


```

## Building mod_cscript

Adjust the settings in the Makefile to suit your distribution

the default make target will build and install the module

## Running the examples

After configuring Apache and installing the module, copy the content in the 
examples directory into the subdirectory configured in the Directory tag. Before 
running the session or database examples you should run prepare the database first!
see db directory

## C-Script API

A brief rundown of functions available to c-script (cscript.h) can be found 
 [here](api-doc/)

__NB__ The API will likely suffer breaking changes in future, all the API
functions use void or char pointers to avoid needing to import apr headers which 
runs a cart and horses through type safety but does simplify things, I'm 
currently undecided how to balance these two factors.


## List of examples

* example one - Simple page content using an XML DOM
* example two - More complex DOM creation and embedded JavaScript
* example three - POST requests
* example four - Basic sessions
* example five - Database access
* example six - Prepared database queries
* example seven - GET requests
* example eight - using postgres
* example nine - server side redirection
* example ten - creation and checking of passphrase hashes
* test app - demonstrate user logins and paginated sql queries

## TODO

Plenty! search through the source code for __TODO__ this is not an exhaustive list...

## FAQ

Why?

Other than why not, this project was originally created for my own edutainment 
initially as an exersise in learning how Apache modules are created and later on 
to test out the powerful facilities of libtcc a part of the 
[tiny C compiler](https://bellard.org/tcc/) 

Compiled or interpreted?

The C source files are compiled on the fly into memory just as they are served, 
this means they can be edited and immediately "run" as if you 
were dealing with an interpreted language.

