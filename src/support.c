
#include <http_protocol.h>
#include "http_ssl.h"
#include <http_log.h>
#include <util_script.h>

#include "mod_session.h"

#include <apr_strings.h>
#include "apr_dbd.h"
#include "apr_env.h"
#include "apr_uuid.h"
#include "apr_strings.h"
#include "apr_pools.h"

#include <libxml/tree.h>

//#define __USE_GNU       // crypt_r (defined somewhere else)
#include <crypt.h>

#include <stdarg.h>
#include "support.h"


extern xmlNodePtr title;
extern xmlDocPtr doc;

static request_rec* saved_r;
static int is_post = 0;
static apr_table_t* posttab;
static apr_table_t* gettab;

char* redir = NULL; // if set redirect url for outgoing header
void* sess_destroy = NULL;

// in exchange for a NULL in some routines return this...
static char* blank = "";

void populateRequestVars() 
{
    posttab = populatePostVars(saved_r);
    ap_args_to_table(saved_r, &gettab); 
}

static void addsrc(xmlNodePtr node, char* content) 
{
    xmlNewProp(node, "type", "text/javascript");
    xmlNodePtr src = xmlNewCDataBlock(doc, content, strlen(content));
    xmlAddChild(node, src);    
}

void setRequest(request_rec* r) 
{
    saved_r = r;
}

//request_rec* getRequest() // used by TCC error callback
//{
//    return saved_r;
//}

void* addTag(void* parent, char* name, char* content) 
{
    if (apr_strnatcasecmp("script",name)!=0 || !content) {
        return xmlNewChild(parent, NULL, name, content);
    } else {
        xmlNodePtr node = xmlNewChild(parent, NULL, name, NULL);
        addsrc(node, content);  
        return node; 
    }
}

char* getHost() 
{
    return (char*)saved_r->hostname;
}

char* getURI() 
{
    return saved_r->unparsed_uri;
}

void redirect(char* uri) 
{
    redir = uri;
}

int isPost() 
{
    if (saved_r->method_number == M_POST) {
        return 1;
    } else {
        return 0;
    }
}

int isSSL() 
{
    return ap_ssl_conn_is_ssl(saved_r->connection);
}

void* hashdata = NULL;

char* dohash(char* phrase, char* check) 
{
    if (!hashdata) {
        hashdata = apr_palloc(saved_r->pool, sizeof(struct crypt_data));
        memset(hashdata, 0, sizeof(struct crypt_data));
    }
    
    if (!check) {
        unsigned char ubytes[16];
        const char *const saltchars =
            "./0123456789ABCDEFGHIJKLMNOPQRST"
            "UVWXYZabcdefghijklmnopqrstuvwxyz";
        char salt[20] = {0};
        char *hash;

        apr_generate_random_bytes(ubytes, sizeof(ubytes));
        salt[0] = '$';
        salt[1] = '6'; // SHA-512 
        salt[2] = '$';
        for (int i = 0; i < 16; i++) {
            salt[3+i] = saltchars[ubytes[i] & 0x3f];
        }

        hash = crypt_r(phrase , salt, hashdata);
        if (!hash || hash[0] == '*') {
            //perror ("crypt");
            // TODO look up errno and log error string
            return NULL;
        }  
        return hash;  
    } else {
        char* chk = crypt_r(phrase , check, hashdata);
        if (!chk || chk[0] == '*') {
            //perror ("crypt");
            // TODO look up errno and log error string
            return NULL;
        }  
        return chk;
    }
}

void setTextContent(void* node, char* content) 
{
    if (apr_strnatcasecmp("script", ((xmlNode*)node)->name)!=0) {
        xmlNodeSetContent(node, content);
    } else {
        addsrc(node, content);  
    }    
}

void changeTitle(char* titleText) 
{
    xmlNodeSetContent(title, titleText);
}

void setAttrib(void* node, char* attribute, char* value) 
{
    xmlSetProp(node, attribute, value); // creates if doesn't exist
}

char* getGet(char* varname) 
{
    return GetPostVar(saved_r, gettab, varname); // TODO function should really be called getTableVar ...
}

char* getPost(char* varname) 
{
    return GetPostVar(saved_r, posttab, varname); 
}

char* getPostIdx(char* varname, int index) 
{
    return GetPostVarIdx(saved_r, posttab, varname, index); 
}

// move into dbcontext ?
static APR_OPTIONAL_FN_TYPE(ap_session_load) *ap_session_load_fn = NULL;
static APR_OPTIONAL_FN_TYPE(ap_session_save) *ap_session_save_fn = NULL;
static APR_OPTIONAL_FN_TYPE(ap_session_get)  *ap_session_get_fn = NULL;
static APR_OPTIONAL_FN_TYPE(ap_session_set)  *ap_session_set_fn = NULL;

// might only need to do this once at module config time ???
void setSessionFuncs() 
{
    ap_session_load_fn = APR_RETRIEVE_OPTIONAL_FN(ap_session_load);
    ap_session_save_fn = APR_RETRIEVE_OPTIONAL_FN(ap_session_save);
    ap_session_get_fn = APR_RETRIEVE_OPTIONAL_FN(ap_session_get);
    ap_session_set_fn = APR_RETRIEVE_OPTIONAL_FN(ap_session_set);
}

char* dumpSess(void* sess)
{
    session_rec* z = (session_rec*)sess;
    char* us = apr_palloc(saved_r->pool, APR_UUID_FORMATTED_LENGTH+1);
    apr_uuid_format(us, z->uuid);
    char* str = apr_psprintf(saved_r->pool, "encoded session: %s %s", 
                           us, z->encoded, 0);    
    return str;
}

unsigned long now() 
{
    return apr_time_now();
}

void* loadSession() 
{
    session_rec *z = NULL;
    ap_session_load_fn(saved_r, &z);
    return z;
}

void destroySession(void* sess) 
{
    sess_destroy = sess;
}

void saveSession(void * sess) 
{
    ap_session_save_fn(saved_r, sess);
}

void setSessionVal(void* sess, char* key, char* value) 
{
    ap_session_set_fn(saved_r, sess, key, value);
}

void getSessionVal(void* sess, char* key, char** value) 
{
    ap_session_get_fn(saved_r, sess, key, (const char **)value);
}

int abrt(int ec) 
{
    ap_log_error(APLOG_MARK, APLOG_ERR , 0, saved_r->server, "error creating pool %i",ec); 
    return 0;
}

dbcontext* initDB(char *name) 
{
    apr_dbd_init(saved_r->pool); // this has probably already happened ???
    dbcontext* DBcontext = apr_pcalloc(saved_r->pool, sizeof(dbcontext));

    int rc = apr_dbd_get_driver (saved_r->pool, name, &DBcontext->driver);
    if (rc == APR_SUCCESS) return DBcontext;
    if (rc == APR_ENOTIMPL) ap_log_error(APLOG_MARK, APLOG_ERR , 0, saved_r->server, "apr_dbd no driver"); 
    if (rc == APR_EDSOOPEN) ap_log_error(APLOG_MARK, APLOG_ERR , 0, saved_r->server, "apr_dbd DSO driver file can't be opened");
    if (rc == APR_ESYMNOTFOUND) ap_log_error(APLOG_MARK, APLOG_ERR , 0, saved_r->server, "apr_dbd driver file doesn't contain a driver"); 
    return NULL;      
}



void openDB(dbcontext* ctx, char *params) 
{
    const char* error;

    int rc = apr_dbd_open_ex(ctx->driver, 
                            saved_r->pool, 
                            (const char*)params,
                            &ctx->handle, &error);
    if (rc) {
        ap_log_error(APLOG_MARK, APLOG_ERR , 0, saved_r->server, "db open: %s", error);
    }


}

void* selectDB(dbcontext* ctx, char* stm) 
{ 
    apr_dbd_results_t *res = NULL;
    int rc = apr_dbd_select(ctx->driver,
                        saved_r->pool,
                        ctx->handle,
                        &res,
                        (const char*)stm,
                        1);
    if (rc) {
        ap_log_error(APLOG_MARK, APLOG_ERR , 0, saved_r->server, "db select error: %i", rc);
        res = NULL;
    }
    return (void*)res;
}

int getNumRowsDB(dbcontext* ctx, void* res) 
{
    return apr_dbd_num_tuples(ctx->driver, (apr_dbd_results_t*)res);   
}

int getNumColsDB(dbcontext* ctx, void* res) 
{
    return apr_dbd_num_cols(ctx->driver,
                    (apr_dbd_results_t*)res);   
}

void* getRowDB(dbcontext* ctx, void* res, int rn) 
{
    apr_dbd_row_t* row = NULL;
    int rc = apr_dbd_get_row(ctx->driver,
        saved_r->pool,
        (apr_dbd_results_t*)res,
        &row, rn);
    if (rc) {
        row = NULL;
        ap_log_error(APLOG_MARK, APLOG_ERR , 0, saved_r->server, "db get row error:", rc);
    }
    return (void*)row;
}


char* getFieldDB(dbcontext* ctx, void* row, int col) 
{
    char* v = (char*)apr_dbd_get_entry(ctx->driver,
                                    (apr_dbd_row_t*)row, col); 
    if (!v) v = blank;
    return v;
}

void closeDB(dbcontext* ctx) 
{
    apr_dbd_close(ctx->driver, ctx->handle);    
}

char* escapeHtmlStr(char* s) 
{
    return ap_escape_html(saved_r->pool, s);
}

int queryDB(dbcontext* ctx, char* sql) 
{
    int nr;
    int rc = apr_dbd_query(ctx->driver, ctx->handle, &nr, (const char*)sql);
    if (rc) {
        nr = 0;
        ap_log_error(APLOG_MARK, APLOG_ERR , 0, saved_r->server, "query failed error:%i",rc);
    }
    return nr;
}

void* prepareDB(dbcontext* ctx, char* query) 
{
    apr_dbd_prepared_t *statement;

    int rc = apr_dbd_prepare (ctx->driver, saved_r->pool, 
                        ctx->handle, (const char*)query, 
                        NULL, &statement);    
    if (rc) {
        ap_log_error(APLOG_MARK, APLOG_ERR , 0, saved_r->server, "%i Prepare failed %s",rc, query);
        return NULL;
    }
    return (void*)statement;
}

int pqueryDB(dbcontext* ctx, void* stm, char** args) 
{
    int nr;
    int rc = apr_dbd_pquery(ctx->driver, saved_r->pool,
                            ctx->handle, &nr, (apr_dbd_prepared_t*)stm, 
                            0, (const char **)args);
    if (rc) {
        ap_log_error(APLOG_MARK, APLOG_ERR , 0, saved_r->server, "prepared query failed");
    }
    return nr;
}

char* escapeDB(dbcontext* ctx, char* string)
{
    return (char*)apr_dbd_escape(ctx->driver,
                        saved_r->pool, string, ctx->handle);
}

void setNameDB(dbcontext* ctx, char * name ) 
{
    apr_dbd_set_dbname(ctx->driver,
                saved_r->pool, ctx->handle, name);
}



void* pselectDB(dbcontext* ctx, void* stm, char** args) 
{
    apr_dbd_results_t *res = NULL;
    int rc = apr_dbd_pselect(ctx->driver, saved_r->pool,
                        ctx->handle, &res, (apr_dbd_prepared_t*) stm,
                        1, 0, (const char **)args);
    if (rc) {
        ap_log_error(APLOG_MARK, APLOG_ERR , 0, saved_r->server, "prepared query failed");
        res = NULL;
    }
    return res;
}


//char* error="error";

char* getENV(char* var) 
{
    char* val;
    // apr_get_env won't work here...
    val = (char*)apr_table_get(saved_r->subprocess_env,var); 
    if (!val) return blank;
    return val;
}



apr_table_t* populatePostVars(request_rec *r) 
{
    apr_table_t* tab;
    int res;
    apr_array_header_t* pairs = NULL;
    apr_off_t len;
    apr_size_t size;
    char *buffer;
    
    // get an array of pairs from apache
    res = ap_parse_form_data(r, NULL, &pairs, -1, HUGE_STRING_LEN);
    if (res != OK || !pairs) return NULL; /* Return NULL if we failed or if there are is no POST data */
    // make a table to hold a kind of associated array
    // which is returned by this function
    tab = apr_table_make(r->pool, pairs->nelts +1);
    
    while (pairs && !apr_is_empty_array(pairs)) 
    {
        // pop each pair from the array ready to put into the resuly
        ap_form_pair_t *pair = (ap_form_pair_t *) apr_array_pop(pairs);
        
        
        // copy out the value part of the pair
        apr_brigade_length(pair->value, 1, &len);
        size = (apr_size_t) len;
        buffer = apr_palloc(r->pool, size + 1);
        apr_brigade_flatten(pair->value, buffer, &size);
        buffer[len] = 0;

        // check the name part of the pair to see if its a html form array
        char* rgt = 0;  // ptr to RiGhT square bracket
        char* lft = strchr(pair->name, '[');    // ptr to LeFT square bracket
        char aidx[256]; // the string of the value in the square brackets (Ascii InDeX)
        
        if (lft) { // we have a left bracket
            int osb,csb;    // Open/Close Square Bracket offset
            osb = lft - pair->name;
            rgt = strchr(pair->name, ']');
            csb = rgt - pair->name;
            if (rgt) { // and a right bracket
                int i=0;
                // copy out the index part of the name
                for (char* adr = lft + 1; adr < rgt; adr++) {
                    aidx[i] = adr[0];
                    i++;
                }
                aidx[i] = 0;
                
                // copy out the part of the name before the left bracket
                char* rootname = apr_palloc(r->pool, osb+1);
                apr_cpystrn(rootname, pair->name, osb+1); 
                apr_table_t* arr;
                
                // see if the array has been used before if not
                // create it

                arr = (apr_table_t*)apr_table_get(tab, rootname);
                if (!arr) {
                    arr =  apr_table_make(r->pool, 1);
                    // set the variable array element with the array
                    apr_table_setn(tab, rootname,(char *)arr);
                }
                // the variables array is set with the value
                apr_table_set(arr, aidx, buffer);   
            }
        } else {
            // if its not a html array all we need to do is set the
            // value in the post array 
            apr_table_set(tab, pair->name, buffer);     
        }
    }    

    return tab;
}


// an empty string to to use in place of a NULL
static char zstr[]="\0";

char* GetPostVar(request_rec *r, apr_table_t* tab, char* var) 
{
    char* PostVar = (char*)apr_table_get(tab, var);
    
    if (!PostVar) {
        PostVar = zstr; // either its invalid var name or not even a POST request
    }
    return PostVar;
}

 char* GetPostVarIdx(request_rec *r, apr_table_t* tab, char* var, int idx) 
 {
    apr_table_t* arr = (apr_table_t*)apr_table_get(tab, var);
    if (!arr) return zstr;  // might not even be a post request...
    char *aidx = apr_itoa(r->pool, idx);
    if (!aidx) return zstr;
    char* val = (char*)apr_table_get(arr, aidx);
    if (!val) {  
        val = zstr;  
    }
    return val;
}
 

