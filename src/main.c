
#include <httpd.h>
#include <http_log.h>
#include <http_protocol.h>
#include <http_config.h>



#include <apr_strings.h>
#include <apr_hash.h>
#include <apr_escape.h>
#include "apr_dbd.h"
#include "mod_dbd.h"
#include "mod_session.h"

#include "libtcc.h"

#include <libxml/tree.h>
#include <string.h>

#include "support.h"

extern char* redir;
extern void* sess_destroy;

xmlDocPtr doc;       /* document pointer */
xmlNodePtr title;
xmlNodePtr body;

static char * src;

void setRequest(request_rec* r);
void populateRequestVars();
request_rec* getRequest();

// first function called by a script with this module...
//void foo(char* s) {
//    ap_rprintf(saved_r, "foo says %s", s);
//}

int bail; // TCC error signaler

// part of API
void dumpSrc(char* s) 
{
    sprintf(s,"%s",src);
}

void tcc_error(void* opaque, const char* msg) {
    // TODO apache config to suppress this ?
    char escaped[1024], str[1024];
    apr_escape_entity(escaped, msg+8, APR_ESCAPE_STRING, 0, NULL); // skip <string>
    sprintf(str, "CScript %s line%s", getURI()+1, escaped);
    addTag(body,"div", str);
    bail = 1;
}

typedef struct {
    const char *app_path;         /* basically doc root */
} mod_config;

static mod_config config;

/* Handler for the "AppRoot" directive */
const char *mod_set_path(cmd_parms *cmd, void *cfg, const char *arg)
{
    config.app_path = arg;
    return NULL;
}

// TODO don't need "approot" as serving .c instead, TODO reuse as needed
static const command_rec mod_directives[] =
{
    AP_INIT_TAKE1("CscriptAppRoot", mod_set_path, NULL, RSRC_CONF, "Application root path"),
    { NULL }
};

static ap_dbd_t *(*dbd_acquire_fn) (request_rec *) = NULL;
static void (*dbd_prepare_fn) (server_rec *, const char *, const char *) = NULL;

static int cscript_handler(request_rec* r) 
{
    // handler name in config
    if ( !r->handler || strcmp(r->handler, "cscript_handler") ) {
        return DECLINED ;   /* none of our business */
    }

    if ( r->method_number != M_GET && r->method_number != M_POST) {
        return HTTP_METHOD_NOT_ALLOWED ;  /* Reject other methods */
    }
    
    setRequest(r); // pass request pointer to support routines

    // TODO this should be done once at config time ?
    setSessionFuncs();

    bail = 0; // set by tcc_error

    ap_set_content_type(r, "text/xml; charset=UTF-8") ;
    
    xmlNodePtr root_node = NULL;

    doc = xmlNewDoc(NULL);
    root_node = xmlNewNode(NULL, "html");
    xmlDocSetRootElement(doc, root_node);
    xmlNewProp(root_node, "lang", "en"); // TODO allow altering

    xmlCreateIntSubset(doc, "html", "-//W3C//DTD XHTML 1.0 Transitional//EN", "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd");
    // no longer valid according to html living standard
    //xmlCreateIntSubset(doc, "html", NULL, NULL);
    
    xmlNodePtr head = xmlNewChild(root_node, NULL, "head", NULL);
    title = xmlNewChild(head, NULL, "title", "Cscript");
    body = xmlNewChild(root_node, NULL, "body", NULL);        

    populateRequestVars();

    // clean expired session
    // TODO doing this every request is overkill, need to think
    // of a better trigger, every n requests, after n mins ???
    // or random chance per request ?
    
    if (!dbd_prepare_fn || !dbd_acquire_fn) {  // do once at config time ?
        dbd_prepare_fn = APR_RETRIEVE_OPTIONAL_FN(ap_dbd_prepare);
        dbd_acquire_fn = APR_RETRIEVE_OPTIONAL_FN(ap_dbd_acquire);
    }

/*
    if (dbd_prepare_fn && dbd_acquire_fn) 
    {
        ap_dbd_t *dbd = NULL;
        apr_dbd_prepared_t *statement = NULL;
        
        dbd = dbd_acquire_fn(r);
        if (!dbd) {
            ap_log_error(APLOG_MARK, APLOG_ERR , 0, r->server,
                          "failed to acquire database connection cleaning session");
        } else {
            statement = apr_hash_get(dbd->prepared, "cleansession", APR_HASH_KEY_STRING);
            if (!statement) {
                ap_log_error(APLOG_MARK, APLOG_ERR , 0, r->server,
                              "failed to find the prepared statement called 'cleansession'");
            } else {
                apr_status_t rv;
                int rows = 0; // TODO could info log this is non zero after query?
                apr_int64_t expiry = (apr_int64_t) apr_time_now();
                
                rv = apr_dbd_pvbquery(dbd->driver, r->pool, dbd->handle, &rows,
                            statement, &expiry, NULL);
                if (rv) {
                    ap_log_error(APLOG_MARK, APLOG_ERR , 0, r->server,
                                  "'cleansession' statement failed : %s", apr_dbd_error(dbd->driver, dbd->handle, rv));
                }
            }
            ap_dbd_close(r->server, dbd);
        }
    }
*/

    TCCState *s;
    s = tcc_new();
    
    tcc_set_error_func(s, NULL, tcc_error); // TODO find out what opaque is!
    
    if (!s) {
        ap_log_error(APLOG_MARK, APLOG_ERR , 0, r->server, "Could not create tcc context");
        return HTTP_INTERNAL_SERVER_ERROR;
    }
    
    apr_file_t *file;
    int rc, exists;  
    apr_finfo_t finfo;  
    char *filename;
    filename = apr_pstrdup(r->pool, r->filename); 

    rc = apr_stat(&finfo, filename, APR_FINFO_MIN, r->pool);
    if (rc == APR_SUCCESS) {
        exists = ( (finfo.filetype != APR_NOFILE) &&  !(finfo.filetype & APR_DIR) );
        if (!exists) return HTTP_NOT_FOUND; /* Return a 404 if not found. */
    } else {
        return HTTP_FORBIDDEN;/* If apr_stat failed, we're probably not allowed to check this file. */
    }
    
    rc = apr_file_open(&file, filename, APR_READ, APR_OS_DEFAULT, r->pool);
    
    if (rc == APR_SUCCESS) {
        // allocate finfo.size bytes and read in the file
        src = apr_palloc(r->pool, finfo.size);
        apr_size_t readBytes;
        readBytes = finfo.size;
        
        rc = apr_file_read(file, src, &readBytes);
        src[finfo.size] = 0;
        
        if (rc != APR_SUCCESS) {
            ap_log_error(APLOG_MARK, APLOG_ERR , 0, r->server, "file read failed : %s", filename);
            return HTTP_INTERNAL_SERVER_ERROR;
        }       
    } else {
        ap_log_error(APLOG_MARK, APLOG_ERR , 0, r->server, "open file failed : %s", filename);
        return HTTP_INTERNAL_SERVER_ERROR;
    }    
    apr_file_close(file);

    // find the last slash, to get the path of the file
    // TODO is this robust?
    int len = strlen(filename);
    for (int i=len; i>0; i--) {
        if (filename[i]=='\\' || filename[i]=='/') {
            filename[i] = 0;
            break;
        }
    }

    // filename is now the path
    tcc_add_include_path(s, filename);                    

    tcc_set_output_type(s, TCC_OUTPUT_MEMORY);
    
    if (tcc_compile_string(s, src) == -1) {
        ap_log_error(APLOG_MARK, APLOG_ERR , 0, r->server, "tcc compile failure");
        bail = 1;
    }

    if (!bail) {
        if (tcc_relocate(s, TCC_RELOCATE_AUTO) < 0) {
            ap_log_error(APLOG_MARK, APLOG_ERR , 0, r->server, "tcc relocate failure");
            //return HTTP_INTERNAL_SERVER_ERROR;
        }

        void (*mainfunc)(xmlNodePtr);
        mainfunc = tcc_get_symbol(s, "main");
        if (!mainfunc) {
            ap_log_error(APLOG_MARK, APLOG_ERR , 0, r->server, "could not get script entry point");
            //return HTTP_INTERNAL_SERVER_ERROR;
        } else {
            mainfunc(body);
        }
    }

    if (!redir) {
        xmlChar* mem;
        int size;
        xmlDocDumpFormatMemory(doc, &mem, &size, 1);
        // while the inital XML tag doesn't prevent browsers rendering
        // xhtml validators will fail it...
        ap_rprintf(r, "%s", strstr(mem, "\n")+1); // TODO hack!
        xmlFree(mem);
    }

    /*free the document */
    xmlFreeDoc(doc);
    xmlCleanupParser();
    
    tcc_delete(s);

    if (sess_destroy && dbd_prepare_fn && dbd_acquire_fn) 
    {
        ap_dbd_t *dbd = NULL;
        apr_dbd_prepared_t *statement = NULL;
        
        dbd = dbd_acquire_fn(r);
        if (!dbd) {
            ap_log_error(APLOG_MARK, APLOG_ERR , 0, r->server,
                          "failed to acquire database connection cleaning session");
        } else {
            // DBDPrepareSQL "delete from session where sesskey = %s" deletesession
            statement = apr_hash_get(dbd->prepared, "deletesession", APR_HASH_KEY_STRING);
            if (!statement) {
                ap_log_error(APLOG_MARK, APLOG_ERR , 0, r->server,
                              "failed to find the prepared statement called 'deletesession'");
            } else {
                apr_status_t rv;
                int rows = 0; // TODO log if this is zero after query?
                session_rec* z = (session_rec*)sess_destroy;
                char key[APR_UUID_FORMATTED_LENGTH + 1];
                apr_uuid_format(key, z->uuid);
                z->expiry = apr_time_now();
                rv = apr_dbd_pvbquery(dbd->driver, r->pool, dbd->handle, &rows,
                            statement, key, NULL);
                if (rv) {
                    ap_log_error(APLOG_MARK, APLOG_ERR , 0, r->server,
                                  "'deletesession' statement failed : %s", apr_dbd_error(dbd->driver, dbd->handle, rv));
                }
                if (rows!=1) ap_log_error(APLOG_MARK, APLOG_ERR , 0, r->server,
                          "'deletesession' rows affected : %i", rows);
            }
            sess_destroy = NULL;
            //apr_dbd_close(dbd->driver, dbd->handle); 
            ap_dbd_close(r->server, dbd);
        }
    }
        
    // TODO expand this to send out any server side header
    // note above id redir is set content is not sent only the 
    // header (important!)
    if (redir) {
        apr_table_set(r->headers_out, "Location", redir);
        redir = NULL;
        return HTTP_MOVED_TEMPORARILY;
    }
    
    return OK ;    
 
}





    
/* Hook our handler into Apache at startup */
static void cscript_hooks(apr_pool_t* pool) 
{
    config.app_path = "/srv/apache/www/";
    ap_hook_handler(cscript_handler, NULL, NULL, APR_HOOK_MIDDLE) ;
}

module AP_MODULE_DECLARE_DATA mod_cscript = 
{
    STANDARD20_MODULE_STUFF,
    NULL,
    NULL,
    NULL,
    NULL,
    mod_directives,
    cscript_hooks
};


