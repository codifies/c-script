

typedef struct dbcontext {
    const apr_dbd_driver_t*   driver;        // abstraction driver
    apr_dbd_t*          handle;     // "handle" to the connection
} dbcontext;


char* GetPostVar(request_rec *r, apr_table_t* tab, char* var);
char* GetPostVarIdx(request_rec *r, apr_table_t* tab, char* var, int idx);
apr_table_t* populatePostVars(request_rec *r);
void tcc_error(void* opaque, const char* msg);
void setSessionFuncs();

void* addTag(void* parent, char* name, char* content);
void changeTitle(char* titleText);
void setAttrib(void* node, char* attribute, char* value);
void setTextContent(void* node, char* content);
int isPost();
int isSSL();
char* getHost();
char* getURI();
char* getPost(char* varname);
char* getPostIdx(char* varname, int index);
char* getGet(char* varname);
void* loadSession();
void setSessionVal(void* sess, char* key, char* value);
void getSessionVal(void* sess, char* key, char** value);
void saveSession(void * sess);
void destroySession(void* sess);
char* escapeHtmlStr(char* s);
char* getENV(char* var);
void redirect(char* uri);
char* dohash(char* phrase, char* check);

dbcontext* initDB(char * name);
void openDB(dbcontext* dbctx, char *params);
void* selectDB(dbcontext* dbctx, char* stm);
char* escapeDB(dbcontext* dbctx, char* string);
void setNameDB(dbcontext* dbctx, char * name );
int getNumRowsDB(dbcontext* dbctx, void* res);
void* getRowDB(dbcontext* dbctx, void* res, int rn);
char* getFieldDB(dbcontext* dbctx, void* row, int col);
int getNumColsDB(dbcontext* dbctx, void* res);
void closeDB(dbcontext* dbctx);
int queryDB(dbcontext* dbctx, char* stm);
void* prepareDB(dbcontext* dbctx, char* query);
int pqueryDB(dbcontext* dbctx, void* stm, char** args);
void* pselectDB(dbcontext* dbctx, void* stm, char** args);

char* dumpSess(void* sess);
unsigned long now();

